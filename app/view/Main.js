Ext.define('Sensors.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],

    config: {
        tabBarPosition: 'bottom',

        items: [
            {

                title: 'Compas',
                iconCls: 'home',

                styleHtmlContent: true,

                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Device orientation'
                },
                {
                    xtype: 'panel',
                    id: 'compasPanel'
                }
                ]


            },
            {
                title: 'Location',
                iconCls: 'locate',
		items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Device orientation'
                }]
            }
        ]
    },
   initialize:function(){
      Ext.device.Orientation.on({
         scope: this,
         orientationchange: function(e) {
	        Ext.getCmp('compasPanel').setHtml('<P>Alpha: '+e.alpha+'<br>Beta: '+e.beta+'<br>Gamma: '+e.gamma);

         }
      });
   }
   
});
